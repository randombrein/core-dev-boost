FROM registry.gitlab.com/randombrein/core-dev-gcc5
MAINTAINER Evren KANALICI <evren.kanalici@nevalabs.com>

ENV DEBIAN_FRONTEND noninteractive
ENV BOOST_VERSION 1.61.0
ENV BOOST_SUFFIX 1_61_0


RUN apt-get -qq update && apt-get -qqy --no-install-recommends install \
    make            \
    curl            \
    # boost dependencies
    libbz2-dev      \
&& rm -rf /var/lib/apt/lists/*


# build boost
RUN curl -LO https://sourceforge.net/projects/boost/files/boost/${BOOST_VERSION}/boost_${BOOST_SUFFIX}.tar.gz

# FIXME: re-running update-alternatives!
RUN update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-5 100 --slave /usr/bin/g++ g++ /usr/bin/g++-5

RUN tar -xvzf boost_${BOOST_SUFFIX}.tar.gz >/dev/null 2>&1 \
    && rm boost_${BOOST_SUFFIX}.tar.gz \
    && cd boost_${BOOST_SUFFIX} \
    && chmod +x bootstrap.sh \
	&& ./bootstrap.sh --without-libraries=python --prefix=/usr/local/ \
	&& ./b2 --build-type=complete cxxflags="-fPIC -std=c++14" address-model=64 --layout=versioned -a -j4 linkflags="-fPIC" install >/dev/null 2>&1 \
	&& cd / && rm -rf boost*

